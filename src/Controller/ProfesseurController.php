<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Professeur;
use App\Form\ProfesseurType;

class ProfesseurController extends AbstractController
{
    /**
     * @Route("/professeur", name="professeur")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $professeurs = $em->getRepository(Professeur::class)->findAll();
        return $this->render('professeur/index.html.twig',[
            "professeurs" => $professeurs
        ]);
    }

    /**
     * @Route("/professeur/create", name="professeur_create")
     */
    public function create(Request $request)
    {
        $professeur = new Professeur();
        $form = $this->createForm(ProfesseurType::class, $professeur); 

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isvalid()){
            $professeur = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($professeur);
            $em->flush();
            return $this->redirectToRoute('professeur');

        }
        return $this->render('professeur/create.html.twig',
        [
            'form' => $form->createView()
        ]);     
    }

    /**
     * @Route("/professeur/edit/{id}", name="professeur_edit")
     */
    public function edit(Request $request, int $id){

        $em = $this->getDoctrine()->getManager();
        $professeur = $em->getRepository(Professeur::class)->find($id);
        $form = $this->createForm(ProfesseurType::class, $professeur);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isvalid()){
            $professeur = $form->getData();
            $em->persist($professeur);
            $em->flush();
            return $this->redirectToRoute('professeur');

        }
        return $this->render('professeur/create.html.twig',
        [
            'form' => $form->createView()
        ]);     

    }

    /**
     * @Route("professeur/delete/{id}", name="professeur_delete")
     */
    public function delete(int $id){

        $em = $this->getDoctrine()->getManager();
        $professeur = $em->getRepository(Professeur::class)->find($id);
        $em->remove($professeur);
        $em->flush();
        return $this->redirectToRoute('professeur');

    }
}
