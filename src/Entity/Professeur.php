<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProfesseurRepository")
 */
class Professeur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $nom;

    /**
     * @ORM\Column(type="string")
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", unique=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Matiere", mappedBy="professeurs")
     */
    private $matieres;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Avis", mappedBy="professeurs")
     */
    private $avis;

    public function __construct()
    {
        $this->matieres = new ArrayCollection();
        $this->avis = new ArrayCollection();
    }

    public function __toString(){
        return $this->prenom . ' ' . $this->nom;
    }

    public function toArray()
    {
        return [
        'id'    => $this->getId(),
        'nom'   => $this->getNom(),
        'prenom' => $this->getPrenom(),
        'email' => $this->getEmail(),
        'matiere' => $this->getMatieresArray()

        ];
    }

    public function getMatieresArray(){
        $matiereList = [];

        foreach ($this->getMatieres() as $matiere){
            $matiereList[] = $matiere->getTitre();

        }
        return $matiereList;

    }

    public function getAvisArray(){
        $avisList = [];

        foreach ($this->getAvis() as $avis){
            $avisList[] = [
                'id' => $avis->getId(),
                'note' => $avis->getNote(),
                'commentaire' => $avis->getCommentaire(),
                'emailEtudiant' => $avis->getEmailEtudiant(),

            ];
        }
        return $avisList;
    }

    // ? = peut retourner null
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     *  
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     * 
     * @return self 
     */
    public function setNom( string $nom) :self //designe l'objet
    {
        $this->nom = $nom;
        return $this;//permet le chainage
    }

    /**
     * @return mixed
     *  
     */
    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     * 
     * @return self 
     */
    public function setPrenom(string $prenom) :self
    {
        $this->prenom = $prenom;
        return $this;
    }

    /**
     * @return mixed
     *  
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * 
     * @return self 
     */
    public function setEmail(string $email) :self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     *  
     */
    public function getMatieres():Collection
    {
        return $this->matieres;
    }

    /**
     * @param mixed $matieres
     * 
     * @return self 
     */
    public function setMatieres(Collection $matieres): Professeur
    {
        $this->matieres = $matieres;
        return $this;
    }


    public function addMatiere(Matiere $matiere): self
    {
        if (!$this->matieres->contains($matiere)) {
            $this->matieres[] = $matiere;
        }

        return $this;
    }

    public function removeMatiere(Matiere $matiere): self
    {
        if ($this->matieres->contains($matiere)) {
            $this->matieres->removeElement($matiere);
        }

        return $this;
    }

    /**
     * @return mixed
     *  
     */
    public function getAvis(): Collection
    {
        return $this->avis;
    }

    /**
     * @param mixed $matieres
     * 
     * @return self 
     */
    public function setAvis(Collection $avis) :Professeur
    {
        $this->avis = $avis;
        return $this;
    }
}
