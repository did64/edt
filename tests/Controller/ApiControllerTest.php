<?php
​
namespace App\Tests\Controller;
​
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\Professeur;
use App\Entity\Avis;
​
class ApiControllerTest extends WebTestCase
{
    private $client;
    private $em;
​
    public function setUp() {
        $this->client = static::createClient();
        $this->em = $this->client->getContainer()->get('doctrine.orm.entity_manager');
        $this->loadFixtures();
    }
​
    private function loadFixtures() {
        $this->tearDown();
        $professeur = new Professeur();
        $professeur->setEmail('xavier.godart@gmail.com');
        $professeur->setNom('Godart');
        $professeur->setPrenom('Xabi');
        $this->em->persist($professeur);
        ​
        $avis = new Avis();
        $avis->setNote(5);
        $avis->setEmailEtudiant('fauxprofil@iutbayonne.univ-pau.fr');
        $avis->setProfesseur($professeur);
        $avis->setCommentaire('Excellent professeur.');
        $this->em->persist($avis);
​
        $avis2 = new Avis();
        $avis2->setNote(2);
        $avis2->setEmailEtudiant('jean-michel.aigri@iutbayonne.univ-pau.fr');
        $avis2->setProfesseur($professeur);
        $avis2->setCommentaire('Moyennasse...');
        $this->em->persist($avis2);
​
        $this->em->flush();
    }
​
    public function testGetAllProfesseurs()
    {
        $this->client->request('GET', '/api/professeurs');
        ​
        $this->assertEquals(
            200,
            $this->client->getResponse()->getStatusCode()
        );
​
        $this->assertTrue(
            $this->client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"' // optional message shown on failure
        );
        $this->assertTrue(true);
    }
​
    public function testPostProfesseurs()
    {
        $this->client->request('POST', '/api/professeurs');
        $this->assertEquals($this->client->getResponse()->getStatusCode(), 405);
    }
​
    public function testGetProfesseur()
    {
        $this->client->request('GET', '/api/professeur/1');
        ​
        $this->assertEquals(
            200,
            $this->client->getResponse()->getStatusCode()
        );
​
        $this->assertTrue(
            $this->client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"' // optional message shown on failure
        );
​
        $this->assertContains('Xabi', $this->client->getResponse()->getContent());
    }
​
    public function testGetAllAvisByProfesseur()
    {
        $this->client->request('GET', '/api/professeur/1/avis');
        ​
        $this->assertEquals(
            200,
            $this->client->getResponse()->getStatusCode()
        );
​
        $this->assertTrue(
            $this->client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"' // optional message shown on failure
        );
​
        $this->assertRegexp('/Excellent/', $this->client->getResponse()->getContent());
        $this->assertRegexp('/Moyennasse/', $this->client->getResponse()->getContent());
    }
​
    public function testPostAvisByProfesseur()
    {
        $this->client->request(
            'POST',
            '/api/professeur/1/avis',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{"note": 5, "emailEtudiant": "ymongabu@iutbayonne.univ-pau.fr", "commentaire": "C\'est un pote à Potencier ou quoi???"}'
        );
        ​
        $this->assertEquals(
            201,
            $this->client->getResponse()->getStatusCode()
        );
​
        $this->assertContains('Potencier', $this->client->getResponse()->getContent());
    }
​
    public function testDeleteAvisByProfesseur()
    {
        $this->client->request(
            'DELETE',
            '/api/professeur/avis/1'
        );
        ​
        $this->assertEquals(
            204,
            $this->client->getResponse()->getStatusCode()
        );
    }
​
    public function tearDown()
    {
        $this->purgeTable('App:Professeur');
        $this->purgeTable('App:Avis');
    }
​
    private function purgeTable($className)
    {
        $classMetaData = $this->em->getClassMetadata($className);
        $connection = $this->em->getConnection();
        $dbPlatform = $connection->getDatabasePlatform();
        $connection->beginTransaction();
        try {
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
            $q = $dbPlatform->getTruncateTableSql($classMetaData->getTableName());
            $connection->executeUpdate($q);
            $connection->query('SET FOREIGN_KEY_CHECKS=1');
            $connection->commit();
        }
        catch (\Exception $e) {
            $connection->rollback();
        }
    }
}